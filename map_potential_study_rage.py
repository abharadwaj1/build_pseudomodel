# -*- coding: utf-8 -*-
"""
Created on Sun Oct  4 13:25:17 2020

@author: Alok
"""
# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 19:03:16 2020

@author: abharadwaj1
"""
import numpy as np
import matplotlib.pyplot as plt 
import matplotlib.image as image
from time import time
import mrcfile
import random
from skimage.color import rgb2gray
import math
from celluloid import Camera
import moviepy.editor as mp
import pandas as pd
from sklearn.neighbors import KDTree

point_mass = 1
epsilon = 1
r_mean = 3
capmagnitude_map = 100
capmagnitude_lj = 400
scale_lj = 1
scale_map = 1
delta_time = 0.05
iterrange = [1,50,100,150]
num_points_fraction = 0.1
g = 10
lj_factor = 1.5
slice_index = 12
friction = 15
num_samples = 21

fx = 4
fy = 4
def normalize(X,xmin=0,xmax=1):
    return xmax * ((X-X.min())/(X.max()-X.min())) + xmin


def sum_array_isfinite(a):
    sum_of = 0
    list_a = [x for x in a if np.isfinite(x)]
    return sum(list_a)
        
   
def plot_map_and_selection(map_slice,selection,kicklist,showkicks):
    selection = selection.tolist()
    if showkicks == False:
        [y,x] = [*zip(*selection)]
        plt.imshow(map_slice),plt.scatter(x,y,color='w')
    else:
        kicked = []
        remained = []
        for i in range(len(selection)):
            if i in kicklist:
                kicked.append(selection[i])
            else:
                remained.append(selection[i])
        [yk,xk] = [*zip(*kicked)]
        [yr,xr] = [*zip(*remained)]
        plt.imshow(map_slice),plt.scatter(xr, yr, color='w'),plt.scatter(xk, yk, color='r')
def plot_map_and_points(map_slice,points):
    colors = ['b','g','r','c','m','y','k','w']
    k = 0
    plt.imshow(map_slice)
    for point in points:
        plt.plot(point.position.x,point.position.y,colors[k]+'o')
        k += 1

def plot_point_series_and_map(map_slice,points):
    colors = ['b','g','r','c','m','y','k','w']
    k = 0
    plt.imshow(map_slice)
    for point in points:
        xs = [x for x,y in points[k].position_history]
        ys = [y for x,y in points[k].position_history]
        plt.plot(xs,ys,colors[7])
        plt.plot(xs[0],ys[0],colors[7]+'o')
        plt.plot(xs[-1],ys[-1],colors[7]+'*')
        
        k += 1
def quiver_plot(map_slice,points):
    plt.imshow(map_slice)
    plt.title('Black is acceleration due to LJ, Red is acceleration due to gradient')
    for point in points:
        (X,Y) = point.position.get()
        (U_lj,V_lj) = point.lj_acceleration.get()
        (U_map,V_map) = point.map_acceleration.get()
        
        plt.quiver(X,Y,U_lj,V_lj,color='r',scale=1,scale_units='x')
        plt.quiver(X,Y,U_map,V_map,color='k',scale=1,scale_units='x')
  

# To perform neighbor search
def get_nearest_neighbor_distance(points):
    np_array_points = np.array([list(x.position.get()) for x in points])
    nn_distance = {}
    tree = KDTree(np_array_points)
    for i in range(len(points)):
        d,ind = tree.query(np_array_points[i:i+1],k=2)
        nn_distance[i] = d[0][1]
    
    return nn_distance

def number_of_too_close_neighbors(points,threshold_distance):
    nn_distance = get_nearest_neighbor_distance(points)
    kicklist = [x for x in nn_distance.keys() if nn_distance[x] < threshold_distance]
    number_too_close = len(kicklist)
    return number_too_close

def get_neighborhood(selection):
    neighbors = {}
    tree = KDTree(selection)
    for i in range(len(selection)):
        d,ind = tree.query(selection[i:i+1],k=2)
        neighbors[i] = d[0][1]
    return neighbors

def find_and_kick(selection,kicklist,kick,voxelsize):
    N = len(kicklist)
    
    for i in range(N):
        selection[kicklist[i]]+=[random.randint(-kick,kick)*voxelsize,random.randint(-kick,kick)*voxelsize]
    return selection    
def get_total_map_potential(selection,map_slice):
    potential = 0
    for point in selection:
        x,y = int(point[0]),int(point[1])
        potential += map_slice[y,x]
    return potential

mrc = mrcfile.open('test_map.mrc')
emmap = mrc.data
voxelsize = 1

map_slice = normalize(emmap[:,:,slice_index],0,100)

mrc = mrcfile.open('test_mask.mrc')
mask = mrc.data
mask_slice = mask[:,:,slice_index]

(width,height) = map_slice.shape
all_inside_mask = np.asarray(np.where(mask_slice==1)).T.tolist()

gy,gx = np.gradient(map_slice)
gradient_magnitude=np.sqrt(gx**2 + gy**2)
masked_gradient = gradient_magnitude * mask_slice

num_points = int(len(all_inside_mask) * num_points_fraction)


colors = ['b','g','r','c','m','y','k','w']
k = 0
dt = delta_time
study_list_map_potential = []
study_list_num_kicklist = []
control_list_map_potential = []
control_list_num_kicklist = []

# First the control - without iterations 
for sample in range(num_samples):
    selection = np.array(random.sample(all_inside_mask,num_points))
    control_list_map_potential.append(get_total_map_potential(selection,map_slice))
    neighbors = get_neighborhood(selection)
    kicklist = [x for x in neighbors.keys() if neighbors[x] < r_mean ]
    control_list_num_kicklist.append(len(kicklist))

# Now the actual study - with iterations    
for sample in range(num_samples):
    
    if sample == 0:
        selection = np.load('random_selection_1.npy')
        fig = plt.figure()
        camera = Camera(fig)
        for tot_iteration in range(150):
    #        print(tot_iteration)
            if len(kicklist) > 0:
                plot_map_and_selection(map_slice,selection,kicklist,showkicks=True)
            else:
                plot_map_and_selection(map_slice,selection,kicklist,showkicks=False)
            camera.snap()        
            neighbors = get_neighborhood(selection)
            kicklist = [x for x in neighbors.keys() if neighbors[x] < r_mean ]
            selection = find_and_kick(selection,kicklist,1,1)
            num_kicklist = len(kicklist)
            map_potentials = get_total_map_potential(selection,map_slice)
            
    else: 
        selection = np.array(random.sample(all_inside_mask,num_points))
        for tot_iteration in range(150):
    #        print(tot_iteration)
            neighbors = get_neighborhood(selection)
            kicklist = [x for x in neighbors.keys() if neighbors[x] < r_mean ]
            selection = find_and_kick(selection,kicklist,1,1)
            num_kicklist = len(kicklist)
            map_potentials = get_total_map_potential(selection,map_slice)
        
    study_list_map_potential.append(map_potentials)
    study_list_num_kicklist.append(num_kicklist)
    
plt.figure()
plt.boxplot([control_list_map_potential,study_list_map_potential]);
plt.figure()
plt.boxplot([control_list_num_kicklist,study_list_num_kicklist]);
 

print("Num of points = "+str(num_points))
print("Mean gradient from map = "+str(masked_gradient.mean()))
print("Expected increase in map potential from mean gradient = "+str(masked_gradient.mean()*num_points))

animation = camera.animate()
outfilename = 'find_and_kick_visualised_1'
animation.save(outfilename+'.gif')
clip = mp.VideoFileClip(outfilename+'.gif')
clip.write_videofile(outfilename+'.mp4')