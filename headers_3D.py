import numpy as np
import string
import gemmi
import matplotlib.pyplot as plt 
import matplotlib.image as image
from time import time
import mrcfile
import random
import math
from scipy.constants import Avogadro
import pandas as pd
from sklearn.neighbors import KDTree
from scipy import spatial

def calculate_number_of_atoms(mask,pd=1.35):
    '''
    To calculate number of atoms based on protein density 
    '''
    all_inside_mask = np.asarray(np.where(mask==1)).T.tolist()  
    total_voxels = len(all_inside_mask)
    mw = 16 #g/mole
    vsize = 1.2156 #voxelsize
    ang_to_cm = 1e-8
    mask_vol = total_voxels * (vsize*ang_to_cm)**3
    mask_mass = mask_vol * pd
    mass_atom = mw / Avogadro
    num_atoms = round(mask_mass / mass_atom)
    
    return num_atoms

def add_residue(model, chain_num, res_num):
    model[chain_num].add_residue(gemmi.Residue(),res_num)
    model[chain_num][res_num].name = 'HOH'
    model[chain_num][res_num].seqid.num = res_num

    return model

def add_atom(model, chain_num, res_num, atom_num,position,voxelsize):
    if len(position) == 2:
        position = np.array([position[0],position[1],0])
    atom = gemmi.Atom()
    atom.name = 'O'
    atom.element = gemmi.Element('O')
    atom.pos = gemmi.Position(position[0]*voxelsize,position[1]*voxelsize,position[2]*voxelsize)
    model[chain_num][res_num].add_atom(atom,atom_num)

    return model

def write_pdb(pseudomodel,output_string):
    structure = gemmi.Structure()
    structure.add_model(pseudomodel)
    structure.write_pdb(output_string)

def convert_to_gemmi_model(points,voxelsize=1):
    np_points = np.array([x.position.get() for x in points])
    model = gemmi.Model('pseudo')
    chain_letters = list(string.ascii_uppercase)
    chain_count = 0
    res_count = 0
    atom_count = 0
    model.add_chain(chain_letters[chain_count])
    model = add_residue(model,chain_count,res_count)
    for point in np_points:
        model = add_atom(model,chain_count,res_count,atom_count,point,voxelsize)
        res_count += 1
        atom_count += 1
        model = add_residue(model,chain_count,res_count)
        if atom_count % 9999 == 0:
            chain_count += 1
            model.add_chain(chain_letters[chain_count])
            res_count = 0
            model = add_residue(model,chain_count,res_count)
    
    return model

class Vector:
    def __init__(self,input_array):
        self.x = input_array[0]
        self.y = input_array[1]
        self.z = input_array[2]
    def get(self):
        return np.array([self.x,self.y,self.z])
    def magnitude(self):
        return math.sqrt(self.x**2+self.y**2+self.z**2)
    def cap_magnitude(self,cap):
        mag = self.magnitude()
        if mag > cap:
            factor= cap/mag
            return self.scale(factor)
        else:
            return self
    
    def scale(self,scale):
        return Vector(scale*self.get())

def add_Vector(vector_a,vector_b):
        return Vector(vector_a.get() + vector_b.get())

d_type = [('pos',tuple),('vel',tuple),('acc',tuple)]
class PointClass:
    def __init__(self,init_pos):
        self.id = 0
        self.position = Vector(init_pos)
        self.velocity = Vector(np.array([0,0,0]))    
        self.acceleration = Vector(np.array([0,0,0]))
        self.mass = 1 # Mass factor - not in real units! 
        
        self.map_value = Vector(np.array([0,0,0]))
        self.position_history = [self.position.get()]
        self.velocity_history = [self.velocity.get()]
        self.acceleration_history = [self.acceleration.get()]
        self.map_value_history = [self.map_value]
    def get_distance_vector(self,target):
        distance_vector = Vector(np.array(self.position.get() - target.position.get()))
        return distance_vector
    
    def angle_wrt_horizontal(self,target):
        return math.atan2(target.position.y - self.position.y, target.position.x - self.position.x)
    
    def velocity_from_acceleration(self,dt):
        vx = self.velocity.x + self.acceleration.x*dt
        vy = self.velocity.y + self.acceleration.y*dt
        vz = self.velocity.z + self.acceleration.z*dt
       # print('velocity: '+str(tuple([vx,vy])))
        self.velocity = Vector(np.array([vx,vy,vz]))
        
    def position_from_velocity(self,dt):
        x = self.position.x + self.velocity.x*dt
        y = self.position.y + self.velocity.y*dt
        z = self.position.z + self.velocity.z*dt
        self.position = Vector(np.array([x,y,z]))
    
    def update_history(self):
        self.position_history.append(self.position.get())
        self.velocity_history.append(self.velocity.get())
        self.acceleration_history.append(self.acceleration.get())
        self.map_value_history.append(self.map_value)

        
def get_acceleration_from_gradient(gx,gy,gz,emmap,g,point,voxelsize):
    tic = time()
    [x,y,z] = [int(point.position.x),int(point.position.y),int(point.position.z)]
    theta_x = gx[z,y,x] / voxelsize
    theta_y = gy[z,y,x] / voxelsize
    theta_z = gz[z,y,x] / voxelsize
    
    acceleration_x = g * theta_x
    acceleration_y = g * theta_y
    acceleration_z = g * theta_z
    #print(point.position.get())
    #print((acceleration_x,acceleration_y))
    
    acceleration = Vector(np.array([acceleration_x,acceleration_y,acceleration_z]))
    #return acceleration,map_slice[y,x]
    time_map.append(time()-tic)
    return acceleration.cap_magnitude(capmagnitude_map),emmap[z,y,x]


def get_acceleration_from_lj_potential(targetpoint,lj_neighbors):
    tic = time()

    lj_neighbors_points = [x.position.get() for x in lj_neighbors]
    distance_vector = targetpoint.position.get() - lj_neighbors_points
    r = np.sqrt(np.einsum('ij->i',distance_vector**2))
    unit_diff_vector = (distance_vector.transpose() / r).transpose()

    
    eps = epsilon
    rm = r_mean*lj_factor
    #print(r[0])
  #  unit_diff_vector = np.array(unit_diff_vector)
    v_lj = eps * ((rm/r)**12 - 2*(rm/r)**6)
   # print(unit_diff_vector.shape)
    #force from v_lj, f=dv/dr
    
    f_r = np.array((12 * eps * rm**6 * (r**6 - rm**6))/r**13)
    
    #print(type(f_r))
    #print(type(unit_diff_vector))
    
    f_r_vector = np.array([np.array(f_r[k])*np.array(unit_diff_vector[k]) for k in range(len(lj_neighbors))])
    #print(f_r_vector)
    fx = -f_r_vector[:,0]
    fy = -f_r_vector[:,1]
    fz = -f_r_vector[:,2]

    
    ax = fx.sum() / targetpoint.mass
    ay = fy.sum() / targetpoint.mass
    az = fz.sum() / targetpoint.mass
    acc = Vector(np.array([ax,ay,az]))

    time_lj.append(time()-tic)
    return acc.cap_magnitude(capmagnitude_lj),v_lj.sum()

def get_neighborhood(points,min_dist):
    tic = time()
    np_points = np.array([list(x.position.get()) for x in points])
    neighborhood = {}
    
    tree = KDTree(np_points)
    for i in range(len(points)):
        ind = tree.query_radius(np_points[i:i+1],r=min_dist*3)[0]
        d,ix = tree.query(np_points[i:i+1],k=2)
        #print(d[0][1])
        #print(ind[0])
        ind = np.delete(ind,np.where(ind==i))
        neighborhood[i]=[d[0][1],ind]
    #number_of_contacts = tree.two_point_correlation(np_points,r_mean)

    return neighborhood

def average_map_value(points):
    sum_map_value = 0
    for point in points:
        sum_map_value += point.map_value
    average_mapvalue = sum_map_value/len(points)
    return average_mapvalue



def main_solver(emmap,gx,gy,gz,points,g,friction,min_dist,voxelsize,dt=0.05,capmagnitude_lj=400,epsilon=1,scale_lj=1,capmagnitude_map=100,scale_map=1,total_iterations=50):
    number_of_contacts = []
    map_values = []
    for iter in range(total_iterations):
        neighborhood = get_neighborhood(points,min_dist)
        small_distances = [d[0] for d in neighborhood.values() if d[0] <= min_dist]
        number_of_contacts.append(len(small_distances))
    
        point_id = 0
        for point in points:
            lj_neighbors = [points[k] for k in neighborhood[point_id][1]]
            
            gradient_acceleration,map_value = get_acceleration_from_gradient(gx,gy,gz,emmap, g, point=point, voxelsize=voxelsize)
            if len(lj_neighbors)==0:
                lj_potential_acceleration,lj_potential = Vector(np.array([0,0,0])),0
            else:
                lj_potential_acceleration,lj_potential = get_acceleration_from_lj_potential(point, lj_neighbors)
            
            gradient_acceleration,lj_potential_acceleration = gradient_acceleration.scale(scale_map),lj_potential_acceleration.scale(scale_lj)
            acceleration = add_Vector(gradient_acceleration,lj_potential_acceleration)
            # add friction 
            point.acceleration = add_Vector(acceleration, point.velocity.scale(-friction))
            point.map_value = map_value
            point_id += 1
        
        map_values.append(average_map_value(points))
    
        for point in points:
            point.velocity_from_acceleration(dt)        
            point.position_from_velocity(dt)
            point.update_history()
    
    print(str(iter)+": Num kicked = "+str(number_of_contacts[iter])+": Avg map potential= "+str(map_values[iter]))    
    
    return points
