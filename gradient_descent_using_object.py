# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import numpy as np
import matplotlib.pyplot as plt 
import matplotlib.image as image
from time import time
import mrcfile
import random
from skimage.color import rgb2gray
import math

amu_to_kg = 1.6605e-27

def normalize(X,xmin=0,xmax=1):
    return xmax * ((X-X.min())/(X.max()-X.min())) + xmin

class Vector:
    def __init__(self,input_tuple):
        self.x = input_tuple[0]
        self.y = input_tuple[1]
    def get(self):
        return (self.x,self.y)
    
    
    def add_to(self,target):
        return Vector((self.x+target.x,self.y+target.y))

class PointClass:
    def __init__(self,init_pos):
        self.position = Vector(init_pos)
        self.velocity = Vector((0,0))    
        self.acceleration = Vector((0,0))
        self.mass = 16 * amu_to_kg #Mass measured in amu.. 
        
    def distance_to(self,target):
        return math.sqrt((self.position.x-target.position.x)**2+(self.position.y-target.position.y)**2)
    
    def velocity_from_acceleration(self,dt):
        vx = self.velocity.x + self.acceleration.x*dt
        vy = self.velocity.y + self.acceleration.y*dt
       # print('velocity: '+str(tuple([vx,vy])))
        self.velocity = Vector((vx,vy))
    def position_from_velocity(self,dt,voxelsize):
        x = int((self.position.x + self.velocity.x*dt)*voxelsize)
        y = int((self.position.y + self.velocity.y*dt)*voxelsize)
      #  print('position: '+str(tuple([x,y])))
        self.position = Vector((x,y))

def get_acceleration_from_gradient(gx,gy,g,point,voxelsize):
    [x,y] = [point.position.x,point.position.y]
    theta_x = gx[y,x] / voxelsize
    theta_y = gy[y,x] / voxelsize
    
    acceleration_x = g * theta_x
    acceleration_y = g * theta_y
    #print(point.position.get())
    return Vector((acceleration_x,acceleration_y))

def get_acceleration_from_each_other
    
def get_gradient_path(point,map_slice,voxelsize,g=3,N=50):
    [xi,yi] = [point.position.x,point.position.y]
    gy,gx = np.gradient(map_slice)
    #[xi,yi] = [24,20]
    xs = []
    ys = []
    potential = []
    dt = 1e-2
    time_tot = dt*N
    for iter in range(N):
        #print("("+str(xi)+","+str(yi)+")")
        xs.append(xi)
        ys.append(yi)

        point.acceleration = get_acceleration_from_gradient(gx,gy,g,point,voxelsize)
        print(point.acceleration.get())
        point.velocity_from_acceleration(dt)
        #print(point.velocity.get())
        point.position_from_velocity(dt,voxelsize)
        #print("("+str(gxi)+","+str(gyi)+")")
        xi = point.position.x
        yi = point.position.y
        potential.append(map_slice[yi,xi])
    return xs,ys,potential


mrc = mrcfile.open('test_map.mrc')
emmap = mrc.data
voxelsize = 1

map_slice = normalize(emmap[:,:,12])

mrc = mrcfile.open('test_mask.mrc')
mask = mrc.data
mask_slice = mask[:,:,12]


(width,height) = map_slice.shape
all_inside_mask = np.asarray(np.where(mask_slice==1)).T.tolist()

gy,gx = np.gradient(map_slice)

num_points = 8
points = [PointClass(tuple([x[1],x[0]])) for x in random.sample(all_inside_mask,num_points)]
colors = ['b','g','r','c','m','y','k','w']
k = 0
plt.imshow(map_slice)
potential = {}
for point in points:
    c = colors[k]
    xs,ys,potential[k] = get_gradient_path(point,map_slice,voxelsize,g=0.05,N=10)
    plt.plot(xs,ys,c)
    plt.plot(xs[0],ys[0],c+'o')
    plt.plot(xs[-1],ys[-1],c+'x')    
    k += 1

