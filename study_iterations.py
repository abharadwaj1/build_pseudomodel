# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 19:03:16 2020

@author: abharadwaj1
"""
import numpy as np
import matplotlib.pyplot as plt 
import matplotlib.image as image
from time import time
import mrcfile
import random
from skimage.color import rgb2gray
import math
from celluloid import Camera
import moviepy.editor as mp
import pandas as pd
from sklearn.neighbors import KDTree

point_mass = 1
epsilon = 1
r_mean = 1.5
capmagnitude_map = 100
capmagnitude_lj = 400
scale_lj = 1
scale_map = 1
delta_time = 0.05
iterrange = [1,50,100,150]
num_points_fraction = 0.1
g = 10
lj_factor = 1.5
slice_index = 12
friction = 15
num_samples = 21

fx = 4
fy = 4
def normalize(X,xmin=0,xmax=1):
    return xmax * ((X-X.min())/(X.max()-X.min())) + xmin

class Vector:
    def __init__(self,input_tuple):
        self.x = input_tuple[0]
        self.y = input_tuple[1]
    def get(self):
        return (self.x,self.y)
    def magnitude(self):
        return math.sqrt(self.x**2+self.y**2)
    def cap_magnitude(self,cap):
        mag = self.magnitude()
        if mag > cap:
            factor= cap/mag
            return self.scale(factor)
        else:
            return self
    
    def scale(self,scale):
        return Vector(tuple(scale*np.array(self.get())))

def add_Vector(vector_a,vector_b):
        return Vector(tuple(np.array(vector_a.get()) + np.array(vector_b.get())))

d_type = [('pos',tuple),('vel',tuple),('acc',tuple)]
class PointClass:
    def __init__(self,init_pos):
        self.id = 0
        self.position = Vector(init_pos)
        self.velocity = Vector((0,0))    
        self.acceleration = Vector((0,0))
        self.mass = point_mass # Mass factor - not in real units! 
        
        self.lj_acceleration = Vector((0,0))
        self.map_acceleration = Vector((0,0))
        self.position_history = [self.position.get()]
        self.velocity_history = [self.velocity.get()]
        self.acceleration_history = [self.acceleration.get()]
        self.lj_acceleration_history = [self.lj_acceleration.magnitude()]
        self.map_acceleration_history = [self.map_acceleration.magnitude()]
    def distance_to(self,target):
        return math.sqrt((self.position.x-target.position.x)**2+(self.position.y-target.position.y)**2)
    
    def angle_wrt_horizontal(self,target):
        return math.atan2(target.position.y - self.position.y, target.position.x - self.position.x)
    
    def velocity_from_acceleration(self,dt):
        vx = self.velocity.x + self.acceleration.x*dt
        vy = self.velocity.y + self.acceleration.y*dt
       # print('velocity: '+str(tuple([vx,vy])))
        self.velocity = Vector((vx,vy))
    def position_from_velocity(self,dt,voxelsize):
        x = ((self.position.x + self.velocity.x*dt)*voxelsize)
        y = ((self.position.y + self.velocity.y*dt)*voxelsize)
      #  print('position: '+str(tuple([x,y])))
        self.position = Vector((x%50,y%50))
    
    def update_history(self):
        self.position_history.append(self.position.get())
        self.velocity_history.append(self.velocity.get())
        self.acceleration_history.append(self.acceleration.get())
        self.lj_acceleration_history.append(self.lj_acceleration.magnitude())
        self.map_acceleration_history.append(self.map_acceleration.magnitude())
        
def get_acceleration_from_gradient(map_slice,g,point,voxelsize):
    gy,gx = np.gradient(map_slice)
    [x,y] = [int(point.position.x),int(point.position.y)]
    theta_x = gx[y,x] / voxelsize
    theta_y = gy[y,x] / voxelsize
    
    acceleration_x = g * theta_x
    acceleration_y = g * theta_y
    #print(point.position.get())
    #print((acceleration_x,acceleration_y))
    
    acceleration = Vector((acceleration_x,acceleration_y))
    #return acceleration,map_slice[y,x]
    return acceleration.cap_magnitude(capmagnitude_map),map_slice[y,x]

def sum_array_isfinite(a):
    sum_of = 0
    list_a = [x for x in a if np.isfinite(x)]
    return sum(list_a)
        

def get_acceleration_from_lj_potential(targetpoint,allpoints,voxelsize):
    
    k = 0
    points = [x for x in allpoints if x != targetpoint]
    r = np.zeros(len(points))
    angles = np.zeros(len(points))
    for point in points:
        r[k] = targetpoint.distance_to(point)*voxelsize
        angles[k] = targetpoint.angle_wrt_horizontal(point) #angles in radian
        k += 1
    eps = epsilon
    rm = r_mean*lj_factor
    
    v_lj = eps * ((rm/r)**12 - 2*(rm/r)**6)
    
    #force from v_lj, f=dv/dr
    
    f_r = (12 * eps * rm**6 * (r**6 - rm**6))/r**13
    #print(f_r)
    fx = f_r * np.cos(angles)
    fy = f_r * np.sin(angles)
    
    fx_sum = sum_array_isfinite(fx)
    fy_sum = sum_array_isfinite(fy)
    
    ax = fx_sum / targetpoint.mass
    ay = fy_sum / targetpoint.mass
    acc = Vector((ax,ay))
    #return acc,v_lj.sum()
    return acc.cap_magnitude(capmagnitude_lj),v_lj.sum()

def get_repulsive_acceleration(targetpoint,allpoints):
    k = 0
    points = [x for x in allpoints if x != targetpoint]
    r = np.zeros(len(points))
    angles = np.zeros(len(points))
    for point in points:
        r[k] = targetpoint.distance_to(point)*voxelsize
        angles[k] = targetpoint.angle_wrt_horizontal(point) #angles in radian
        k += 1
    
    
def plot_map_and_points(map_slice,points):
    colors = ['b','g','r','c','m','y','k','w']
    k = 0
    plt.imshow(map_slice)
    for point in points:
        plt.plot(point.position.x,point.position.y,colors[k]+'o')
        k += 1

def plot_point_series_and_map(map_slice,points):
    colors = ['b','g','r','c','m','y','k','w']
    k = 0
    plt.imshow(map_slice)
    for point in points:
        xs = [x for x,y in points[k].position_history]
        ys = [y for x,y in points[k].position_history]
        plt.plot(xs,ys,colors[7])
        plt.plot(xs[0],ys[0],colors[7]+'o')
        plt.plot(xs[-1],ys[-1],colors[7]+'*')
        
        k += 1
def quiver_plot(map_slice,points):
    plt.imshow(map_slice)
    plt.title('Black is acceleration due to LJ, Red is acceleration due to gradient')
    for point in points:
        (X,Y) = point.position.get()
        (U_lj,V_lj) = point.lj_acceleration.get()
        (U_map,V_map) = point.map_acceleration.get()
        
        plt.quiver(X,Y,U_lj,V_lj,color='r',scale=1,scale_units='x')
        plt.quiver(X,Y,U_map,V_map,color='k',scale=1,scale_units='x')
    
def get_simple_landscape(fx,fy,N):
    output = np.zeros((N,N))
    y,x = np.ogrid[0:N,0:N]
    xv,yv = np.meshgrid(x,y) 
    output += np.sin(2*np.pi*fx*xv/N-np.pi/2) + np.sin(2*np.pi*fy*yv/N-np.pi/2)
    return output


# To perform neighbor search
def get_nearest_neighbor_distance(points):
    np_array_points = np.array([list(x.position.get()) for x in points])
    nn_distance = {}
    tree = KDTree(np_array_points)
    for i in range(len(points)):
        d,ind = tree.query(np_array_points[i:i+1],k=2)
        nn_distance[i] = d[0][1]
    
    return nn_distance

def number_of_too_close_neighbors(points,threshold_distance):
    nn_distance = get_nearest_neighbor_distance(points)
    kicklist = [x for x in nn_distance.keys() if nn_distance[x] < threshold_distance]
    number_too_close = len(kicklist)
    return number_too_close



mrc = mrcfile.open('test_map.mrc')
emmap = mrc.data
voxelsize = 1#mrc.voxel_size.x

map_slice = normalize(emmap[:,:,slice_index],0,100)

mrc = mrcfile.open('test_mask.mrc')
mask = mrc.data
mask_slice = mask[:,:,slice_index]

(width,height) = map_slice.shape
all_inside_mask = np.asarray(np.where(mask_slice==1)).T.tolist()

gy,gx = np.gradient(map_slice)
gradient_magnitude=np.sqrt(gx**2 + gy**2)
masked_gradient = gradient_magnitude * mask_slice

num_points = int(len(all_inside_mask) * num_points_fraction)


colors = ['b','g','r','c','m','y','k','w']
k = 0
dt = delta_time

map_potential_list = []
lj_potential_list = []
num_kicklist_list = []
time_taken = []

for tot_iteration in iterrange:
    print(tot_iteration)
    map_potentials = []
    lj_potentials = []
    num_kicklist = []

    tic = time()
    for samples in range(num_samples):
        points = [PointClass(tuple([x[1],x[0]])) for x in random.sample(all_inside_mask,num_points)]
        print(samples)
        for iter in range(tot_iteration):
            sum_of_map_potential = 0
            sum_of_lj_potential = 0
            tot_potential = 0
        
            for point in points:
                gradient_acceleration,map_potential = get_acceleration_from_gradient(map_slice, g, point=point, voxelsize=voxelsize)
                lj_potential_acceleration,lj_potential = get_acceleration_from_lj_potential(point, points,voxelsize=voxelsize)
                
                gradient_acceleration,lj_potential_acceleration = gradient_acceleration.scale(scale_map),lj_potential_acceleration.scale(scale_lj)
                acceleration = add_Vector(gradient_acceleration,lj_potential_acceleration)
                # add friction 
                point.acceleration = add_Vector(acceleration, point.velocity.scale(-friction))
                
                # Next few lines are for analysis
                point.lj_acceleration = lj_potential_acceleration
                point.map_acceleration = gradient_acceleration
                
                sum_of_map_potential += map_potential 
                sum_of_lj_potential += lj_potential

            for point in points:
                point.velocity_from_acceleration(dt)        
                point.position_from_velocity(dt, voxelsize)
                point.update_history()
                
        num_kicklist.append(number_of_too_close_neighbors(points,r_mean))
        map_potentials.append(sum_of_map_potential)
        lj_potentials.append(sum_of_lj_potential)
        
    map_potential_list.append(map_potentials)
    lj_potential_list.append(lj_potentials)
    num_kicklist_list.append(num_kicklist)
    time_taken.append((time()-tic)/num_samples)

plt.figure()
plt.boxplot(map_potential_list);
plt.figure()
plt.boxplot(num_kicklist_list);
plt.figure()
plt.boxplot(lj_potential_list);
            
all_map_acceleration = [x for x in point.map_acceleration_history for point in points]    
all_lj_acceleration = [x for x in point.lj_acceleration_history for point in points]    

print("Num of points = "+str(num_points))
print("Mean gradient from map = "+str(masked_gradient.mean()))
print("Expected increase in map potential from mean gradient = "+str(masked_gradient.mean()*num_points))