import numpy as np
import matplotlib.pyplot as plt 
import matplotlib.image as image
from time import time
import mrcfile
import random
from skimage.color import rgb2gray
import math
from celluloid import Camera
import moviepy.editor as mp
import pandas as pd
from sklearn.neighbors import KDTree
from scipy import spatial
from scipy import signal
from scipy.constants import Avogadro
#from ipywidgets import interact, interactive, fixed, interact_manual, FloatSlider, IntSlider

def normalize(X,xmin=0,xmax=1):
    return xmax * ((X-X.min())/(X.max()-X.min())) + xmin

class Vector:
    def __init__(self,input_tuple):
        self.x = input_tuple[0]
        self.y = input_tuple[1]
    def get(self):
        return (self.x,self.y)
    def magnitude(self):
        return math.sqrt(self.x**2+self.y**2)
    def cap_magnitude(self,cap):
        mag = self.magnitude()
        if mag > cap:
            factor= cap/mag
            return self.scale(factor)
        else:
            return self
    
    def scale(self,scale):
        return Vector(tuple(scale*np.array(self.get())))

def add_Vector(vector_a,vector_b):
        return Vector(tuple(np.array(vector_a.get()) + np.array(vector_b.get())))

d_type = [('pos',tuple),('vel',tuple),('acc',tuple)]
class PointClass:
    def __init__(self,init_pos):
        self.id = 0
        self.position = Vector(init_pos)
        self.velocity = Vector((0,0))    
        self.acceleration = Vector((0,0))
        self.mass = 1 # Mass factor - not in real units! 
        
        self.lj_acceleration = Vector((0,0))
        self.map_acceleration = Vector((0,0))
        self.lj_potential = 0
        self.map_value = 0
        self.position_history = [self.position.get()]
        self.velocity_history = [self.velocity.get()]
        self.acceleration_history = [self.acceleration.get()]
        self.lj_acceleration_history = [self.lj_acceleration.magnitude()]
        self.map_acceleration_history = [self.map_acceleration.magnitude()]
        self.lj_potential_history = [self.lj_potential]
        self.map_value_history = [self.map_value]
    def distance_to(self,target):
        return math.sqrt((self.position.x-target.position.x)**2+(self.position.y-target.position.y)**2)
    
    def angle_wrt_horizontal(self,target):
        return math.atan2(target.position.y - self.position.y, target.position.x - self.position.x)
    
    def velocity_from_acceleration(self,dt):
        vx = self.velocity.x + self.acceleration.x*dt
        vy = self.velocity.y + self.acceleration.y*dt
       # print('velocity: '+str(tuple([vx,vy])))
        self.velocity = Vector((vx,vy))
    def position_from_velocity(self,dt):
        x = ((self.position.x + self.velocity.x*dt))
        y = ((self.position.y + self.velocity.y*dt))
      #  print('position: '+str(tuple([x,y])))
        self.position = Vector((x,y))
    
    def perturb_position(self,kick=1):
        old_position = np.array(self.position.get())
        new_position = old_position + np.random.randint(-kick,kick+1,size=(1,1))[0]
        self.position = Vector(tuple(new_position))
    
    def update_history(self):
        self.position_history.append(self.position.get())
        self.velocity_history.append(self.velocity.get())
        self.acceleration_history.append(self.acceleration.get())
        self.lj_acceleration_history.append(self.lj_acceleration.magnitude())
        self.map_acceleration_history.append(self.map_acceleration.magnitude())
        self.lj_potential_history.append(self.lj_potential)
        self.map_value_history.append(self.map_value)
        
def get_acceleration_from_gradient(map_2d,g,point,voxelsize,capmagnitude_map=100):
    gy,gx = np.gradient(map_2d)
    [x,y] = [int(point.position.x),int(point.position.y)]
    theta_x = gx[y,x] / voxelsize
    theta_y = gy[y,x] / voxelsize
    
    acceleration_x = g * theta_x
    acceleration_y = g * theta_y
    #print(point.position.get())
    #print((acceleration_x,acceleration_y))
    
    acceleration = Vector((acceleration_x,acceleration_y))
    #return acceleration,map_2d[y,x]
    return acceleration.cap_magnitude(capmagnitude_map),map_2d[y,x]



def get_slice(mrcfilename,slice_index):
    mrc = mrcfile.open(mrcfilename)
    mapdata = mrc.data
    map_slice = mapdata[:,:,slice_index]
    
    return map_slice

def sum_array_isfinite(a):
    sum_of = 0
    list_a = [x for x in a if np.isfinite(x)]
    return sum(list_a)

def calculate_number_of_atoms(mask,pd=1.35):
    '''
    To calculate number of atoms based on protein density 
    '''
    all_inside_mask = np.asarray(np.where(mask==1)).T.tolist()  
    total_voxels = len(all_inside_mask)
    mw = 16 #g/mole
    vsize = 1.2156 #voxelsize
    ang_to_cm = 1e-8
    mask_vol = total_voxels * (vsize*ang_to_cm)**3
    mask_mass = mask_vol * pd
    mass_atom = mw / Avogadro
    num_atoms = round(mask_mass / mass_atom)
    
    return num_atoms        

def get_acceleration_from_lj_potential(targetpoint,allpoints,min_dist=1.5,lj_factor=1.5,capmagnitude_lj=400,epsilon=1):
    
    k = 0
    points = [x for x in allpoints if x != targetpoint]
    r = np.zeros(len(points))
    angles = np.zeros(len(points))
    for point in points:
        r[k] = targetpoint.distance_to(point)
        angles[k] = targetpoint.angle_wrt_horizontal(point) #angles in radian
        k += 1
    eps = epsilon
    rm = min_dist*lj_factor
    
    v_lj = eps * ((rm/r)**12 - 2*(rm/r)**6)
    
    #force from v_lj, f=dv/dr
    
    f_r = (12 * eps * rm**6 * (r**6 - rm**6))/r**13
    #print(f_r)
    fx = f_r * np.cos(angles)
    fy = f_r * np.sin(angles)
    
    fx_sum = sum_array_isfinite(fx)
    fy_sum = sum_array_isfinite(fy)
    
    ax = fx_sum / targetpoint.mass
    ay = fy_sum / targetpoint.mass
    acc = Vector((ax,ay))
    #return acc,v_lj.sum()
    return acc.cap_magnitude(capmagnitude_lj),v_lj.sum()

def map_1d_2d_index(index_1d,shape_2d,xy_format=True):
    ''' Input 1-D idnex, and output corresponding row and col index for a desired 2D shape.
    Desired 2D shape in the format: height x width (rows,cols)
    Output index: (col_index,row_index) which is akin to (x,y) coordinate
    '''
    
    (nrows,ncols) = shape_2d
    row_index = index_1d // ncols
    col_index = index_1d - row_index*ncols
    if xy_format == True:
        return (col_index,row_index)
    else:
        return (row_index,col_index) #in X and Y shape

def get_index_of_maximum_N(array_2d,N):
    ''' 
    Takes a 2d array and returns an array of N indices, of the top N elements in the array
    '''
    
    array_1d = array_2d.flatten()
    max_N_indices = array_1d.argsort()[-N:][::-1]
    max_N_2d_indices = np.array([map_1d_2d_index(k,array_2d.shape) for k in max_N_indices])
    return max_N_2d_indices

def get_image_of_points(np_points,N=50):
    ''' 
    converts a set of coordinates into an image. The image has all zeros
except for the indices corresponding to the coordinates, which has value 
1. 
input: np_points = numpy array of points
otuput: image, of shape (N,N) where N is the length of points
'''

    output = np.zeros((N,N)).flatten()
    for point in np_points:
        x = round(point[0])
        y = round(point[1])
        if y == 50:
            index = int(y*(N-1)+x)
        elif y < 50:
            index = int(y*(N)+x)
        else:
            index = N**2
        output[index] = 1
    return output.reshape((N,N))
    
def plot_current_positions_and_map(map_2d,points,color_index='w'):
    plt.imshow(map_2d)
    for point in points:
        plt.plot(point.position.x,point.position.y,color_index+'.')
            
def plot_position_history_and_map(map_2d,points,color_index='w'):
    colors = ['b','g','r','c','m','y','k','w']
    k = 0
    plt.imshow(map_2d)
    for point in points:
        xs = [x for x,y in points[k].position_history]
        ys = [y for x,y in points[k].position_history]
        plt.plot(xs,ys,color_index)
        plt.plot(xs[0],ys[0],color_index+'o')
        plt.plot(xs[-1],ys[-1],color_index+'*')
        
        k += 1
def quiver_plot(map_2d,points):
    plt.imshow(map_2d)
    plt.title('Black is acceleration due to LJ, Red is acceleration due to gradient')
    for point in points:
        (X,Y) = point.position.get()
        (U_lj,V_lj) = point.lj_acceleration.get()
        (U_map,V_map) = point.map_acceleration.get()
        
        plt.quiver(X,Y,U_lj,V_lj,color='r',scale=1,scale_units='x')
        plt.quiver(X,Y,U_map,V_map,color='k',scale=1,scale_units='x')
    
def get_simple_landscape(fx,fy,N):
    output = np.zeros((N,N))
    y,x = np.ogrid[0:N,0:N]
    xv,yv = np.meshgrid(x,y) 
    output += np.sin(2*np.pi*fx*xv/N-np.pi/2) + np.sin(2*np.pi*fy*yv/N-np.pi/2)
    return output

def min_distance_list(array_1,array_2):
    '''
    computes minimum distance for each element in tree1 to each eleemet in tree2
    array_1 and array_2 are np.ndarray type
    '''
    
    tree = KDTree(array_2)
    min_distances = np.array([])
    for i in range(len(array_1)):
        d,ix = tree.query(array_1[i:i+1],k=1)
        min_distances = np.append(min_distances,d[0][0])
    
    return min_distances

def get_neighborhood(points,min_dist):
    ''' 
    Function to find the indices of nearest neighbors for every point in a list of points. 
    This is to calculate LJ potential for every point in the list of points
    Search is limited to only 3 * min_dist since LJ potential goes to zero beyond that.
    
    Input
    points: python list format, each element in list is PointClass 
    threshold_distance: the van-der waal's distance 
    Output
    neighborhood: type - dictionary. neighborhood[key]=[distance_to_nearest_neighbor,np_array_listofneighbors]
    '''
    tic = time()
    np_points = np.array([list(x.position.get()) for x in points])
    neighborhood = {}
    
    tree = KDTree(np_points)
    for i in range(len(points)):
        ind = tree.query_radius(np_points[i:i+1],r=min_dist*3)[0]
        d,ix = tree.query(np_points[i:i+1],k=2)
        ind = np.delete(ind,np.where(ind==i))
        neighborhood[i]=[d[0][1],ind]
    
    return neighborhood    

# To perform neighbor search
def get_nearest_neighbor_distance(points):
    np_array_points = np.array([list(x.position.get()) for x in points])
    nn_distance = {}
    tree = KDTree(np_array_points)
    for i in range(len(points)):
        d,ind = tree.query(np_array_points[i:i+1],k=2)
        nn_distance[i] = d[0][1]
    
    return nn_distance

def total_map_value(points):
    sum_map_value = 0
    for point in points:
        sum_map_value += point.map_value
    return sum_map_value

def number_of_contacts(points,threshold_distance):
    neighborhood = get_nearest_neighbor_distance(points)
    small_distances = [d for d in neighborhood.values() if d <= threshold_distance]

    return len(small_distances)

def main_solver(map_2d,points_initial,friction,voxelsize=1,total_iterations=100,dt=0.05,scale_lj=1,min_dist=1.5,lj_factor=1,epsilon=1,capmagnitude_lj=400,scale_map=1,g=10,capmagnitude_map=100,return_contacts=False,return_map_value=False,return_type='pointclass'):    
    points =  [PointClass((x.position.get())) for x in points_initial]
    num_contacts_iterations = []
    map_value_iterations = []
    for iteration in range(total_iterations):
        
        num_contacts_iterations.append(number_of_contacts(points, min_dist))
        for point in points:
            gradient_acceleration,map_value = get_acceleration_from_gradient(map_2d, g, point, voxelsize,capmagnitude_map=capmagnitude_map)
            lj_potential_acceleration,lj_potential = get_acceleration_from_lj_potential(point, points,min_dist,lj_factor,capmagnitude_lj,epsilon)
            
            gradient_acceleration,lj_potential_acceleration = gradient_acceleration.scale(scale_map),lj_potential_acceleration.scale(scale_lj)
            acceleration = add_Vector(gradient_acceleration,lj_potential_acceleration)
            # add friction 
            point.acceleration = add_Vector(acceleration, point.velocity.scale(-friction))
            
            # Next few lines are for analysis
            point.lj_acceleration = lj_potential_acceleration
            point.map_acceleration = gradient_acceleration
            point.lj_potential = lj_potential
            point.map_value = map_value
    
        for point in points:
            point.velocity_from_acceleration(dt)        
            point.position_from_velocity(dt)
            point.update_history()
    
        map_value_iterations.append(total_map_value(points))
    
    ### Return methods
    
    if return_type == 'pointclass':
        output = points
    elif return_type == 'array':
        output = np.array([x.position.get() for x in points])
    elif return_type == 'tree':
        np_points = np.array([x.position.get() for x in points])
        output = KDTree(np_points)
    elif return_type == 'scipy_tree':
        np_points = np.array([x.position.get() for x in points])
        output = spatial.KDTree(np_points)
    else:
        output = points
    
    if return_contacts == False and return_map_value == False:
        return output
    elif return_contacts == True and return_map_value == False:
        return output,num_contacts_iterations
    elif return_contacts == False and return_map_value == True:
        return output,map_value_iterations
    else:
        return (output,num_contacts_iterations,map_value_iterations)


def main_solver_interactive(map_2d,points,friction,voxelsize,total_iterations,dt,scale_lj,min_dist,lj_factor,epsilon,capmagnitude_lj,scale_map,g,capmagnitude_map,return_contacts,return_map_value,return_type):    
    num_contacts_iterations = []
    map_value_iterations = []
    plt.figure(figsize=(12,12))
    plt.subplot(2,2,1)
    plot_current_positions_and_map(map_2d, points,'r')
    for iteration in range(total_iterations):
        for point in points:
            gradient_acceleration,map_value = get_acceleration_from_gradient(map_2d, g, point, voxelsize,capmagnitude_map=capmagnitude_map)
            lj_potential_acceleration,lj_potential = get_acceleration_from_lj_potential(point, points,min_dist,lj_factor,capmagnitude_lj,epsilon)
            
            gradient_acceleration,lj_potential_acceleration = gradient_acceleration.scale(scale_map),lj_potential_acceleration.scale(scale_lj)
            acceleration = add_Vector(gradient_acceleration,lj_potential_acceleration)
            # add friction 
            point.acceleration = add_Vector(acceleration, point.velocity.scale(-friction))
            
            # Next few lines are for analysis
            point.lj_acceleration = lj_potential_acceleration
            point.map_acceleration = gradient_acceleration
            point.lj_potential = lj_potential
            point.map_value = map_value
    
        for point in points:
            point.velocity_from_acceleration(dt)        
            point.position_from_velocity(dt)
            point.update_history()
    
        
        num_contacts_iterations.append(number_of_contacts(points, min_dist))
        map_value_iterations.append(total_map_value(points))
            
    '''
    plt.subplot(2,2,2)
    plot_position_history_and_map(map_2d, points,'r')
    plot_current_positions_and_map(map_2d, points,'w')
    plt.subplot(2,2,3)
    plt.plot(num_contacts_iterations)
    plt.title('Number of contacts')
    plt.subplot(2,2,4)
    plt.plot(map_value_iterations)    
    plt.title('Total map value at each location')
'''
    
        
def main_solver_analysis(map_2d,points_initial,reference_array,friction,voxelsize=1,total_iterations=100,dt=0.05,scale_lj=1,min_dist=1.5,lj_factor=1.5,epsilon=1,capmagnitude_lj=400,scale_map=1,g=10,capmagnitude_map=100,return_contacts=False,return_map_value=False,return_type='pointclass'):    
    points =  [PointClass((x.position.get())) for x in points_initial]
    num_contacts_iterations = []
    map_value_iterations = []
    min_distance_iteration = []
    #mean_minimum_distance = []
    #rmsd = []
    im_correlation = []
    for iteration in range(total_iterations):
        #print(iteration)
        np_points = np.array([x.position.get() for x in points])
        #minimum_distance_list = min_distance_list(np_points, reference_array)
        #mean_minimum_distance.append(minimum_distance_list.mean())
        #rmsd.append(np.sqrt(np.sum(minimum_distance_list**2)/len(minimum_distance_list)))

        num_contacts_iterations.append(number_of_contacts(points, min_dist))
        correlate = signal.correlate2d(get_image_of_points(np_points),get_image_of_points(reference_array),mode='same')
        im_correlation.append(correlate.max())
        for point in points:
            gradient_acceleration,map_value = get_acceleration_from_gradient(map_2d, g, point, voxelsize,capmagnitude_map=capmagnitude_map)
            lj_potential_acceleration,lj_potential = get_acceleration_from_lj_potential(point, points,min_dist,lj_factor,capmagnitude_lj,epsilon)
            
            gradient_acceleration,lj_potential_acceleration = gradient_acceleration.scale(scale_map),lj_potential_acceleration.scale(scale_lj)
            acceleration = add_Vector(gradient_acceleration,lj_potential_acceleration)
            # add friction 
            point.acceleration = add_Vector(acceleration, point.velocity.scale(-friction))
            
            # Next few lines are for analysis
            point.lj_acceleration = lj_potential_acceleration
            point.map_acceleration = gradient_acceleration
            point.lj_potential = lj_potential
            point.map_value = map_value
    
        for point in points:
            point.velocity_from_acceleration(dt)        
            point.position_from_velocity(dt)
            point.update_history()
           # if iteration in [51,101]:
           #     point.perturb_position(kick=2)
    
        map_value_iterations.append(total_map_value(points))
    np_points = np.array([x.position.get() for x in points])    
    return np_points,num_contacts_iterations,map_value_iterations,im_correlation