# -*- coding: utf-8 -*-
"""
Created on Thu Oct  8 18:35:40 2020

@author: abharadwaj1
"""
from pam_headers import *
import os,sys
import argparse



progname = os.path.basename(sys.argv[0])
author = 'author: Alok Bharadwaj and Arjen J Jakobi, BN'
version = progname + '0.1'

simple_cmd = "python pseudo_atomic_model.py -em emmap.mrc -ma mask.mrc -N 20000 -ar 0.65 -it 50 -f 15 -dt 0.05"

cmdl_parser = argparse.ArgumentParser(
description='*** PAM: Pseudo Atomic Model *** \n Creates a pseudo atomic model using gradient informations from EM Map and boundary set by the mask' + \
'Example usage: \"{0}\". {1}'.format(simple_cmd, author))
     

cmdl_parser.add_argument('-em', '--emmap', required=True, help='Input filename of mask (in mrc format)')
cmdl_parser.add_argument('-ma', '--mask', required=True, help='Input filename of mask (in mrc format)')
cmdl_parser.add_argument('-N', '--num_atoms', required=True, help='Number of atoms')
cmdl_parser.add_argument('-ar', '--atomic_radius', help='Atomic radius in Angstorm',default=0.657)
cmdl_parser.add_argument('-it', '--iterations', help='Number of iterations',default=50)
cmdl_parser.add_argument('-f', '--friction', help='Friction coefficient',default=15)
cmdl_parser.add_argument('-dt', '--delta_time', help='Time steps for solver',default=0.05)


def prepare_inputs(args):
    emmap_path = args.emmap
    mask_path = args.mask
    num_atoms = np.array(args.num_atoms,dtype=int)
    atomic_radius = np.array(args.atomic_radius,dtype=float)
    total_iterations = np.array(args.iterations,dtype=int)
    friction = np.array(args.friction,dtype=float)
    dt = np.array(args.delta_time,dtype=float)
    outfilename = mask_path[:-4] + '_pseudomodel.pdb'
    
    print(
    '\nMap used: '+args.emmap+
    '\nMask used: '+args.mask+
    '\nOutput filename = '+outfilename+
    '\n'+str(num_atoms)+' Atoms will be placed with minimum distance of '+str(2*atomic_radius.round(2))+' angstorm')

    return emmap_path,mask_path,num_atoms,atomic_radius,total_iterations,friction,dt, outfilename

def get_map_parameters(emmap_path, mask_path):
    mrc = mrcfile.open(emmap_path)
    emmap = mrc.data
    voxelsize = mrc.voxel_size.x
    emmap = emmap.clip(min=0)
    
    gz,gy,gx = np.gradient(emmap)
        
    mask = mrcfile.open(mask_path).data
    print(
    '\nVoxelsize: '+str(voxelsize)+
    '\nVolume of mask: '+str((mask.sum() * voxelsize**3))+ ' A^3')   
    
    max_map_value = emmap.max()
    g = max_map_value * 3 / 26
    return emmap, mask, voxelsize, gx, gy, gz, g

def get_points_from_mask(mask, num_atoms):
    
    all_inside_mask = np.asarray(np.where(mask==1)).T.tolist()
    points = [PointClass(np.array([x[2]*voxelsize,x[1]*voxelsize,x[0]*voxelsize])) for x in random.sample(all_inside_mask,num_atoms)]
    
    return points
       


def launch_pam(args):
    emmap_path, mask_path, num_atoms, atomic_radius, total_iterations, friction, dt, outfilename = prepare_inputs(args)
    min_dist = 2 * atomic_radius
    emmap, mask, voxelsize, gx, gy, gz, g = get_map_parameters(emmap_path,mask_path)
    points = get_points_from_mask(mask, num_atoms)
    
    arranged_points = main_solver3D(
        emmap,gx,gy,gz,points,g=g,friction=friction,min_dist=min_dist,voxelsize=voxelsize,dt=dt,total_iterations=total_iterations)
        
    gemmi_model = convert_to_gemmi_model(arranged_points,1);
    write_pdb(gemmi_model,outfilename)
    
    print(" *** PAM step completed! *** ")
    

def main():
    args = cmdl_parser.parse_args()
    launch_pam(args)

if __name__ == '__main__':
    main()
    