# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 19:03:16 2020

@author: abharadwaj1
"""
import numpy as np
import matplotlib.pyplot as plt 
import matplotlib.image as image
from time import time
import mrcfile
import random
from skimage.color import rgb2gray
import math
from celluloid import Camera
import moviepy.editor as mp
import pandas as pd
from sklearn.neighbors import KDTree
from scipy import spatial
from headers import calculate_number_of_atoms
from headers_3D import *
import os,sys
from subprocess import call, Popen, PIPE, run

point_mass = 1
epsilon = 1
r_mean = 1.5
capmagnitude_map = 100
capmagnitude_lj = 400
scale_lj = 1
scale_map = 1
delta_time = 0.05
iterrange = 30
slice_index = 12
protein_density = 1.35
lj_factor = 1.5
#point1 = tuple(np.random.randint(5,45,(1,2))[0])
#point2 = tuple(np.random.randint(5,45,(1,2))[0])
#point1 = (25,34)
#point2 = (25,32)
g = 6

friction = 15

time_lj = []
time_map = []
time_neighbor = []
def normalize(X,xmin=0,xmax=1):
    return xmax * ((X-X.min())/(X.max()-X.min())) + xmin

def compute_radial_profile(vol, center=[0,0,0], return_indices=False):
    dim = vol.shape
    m = np.mod(vol.shape,2)
    # make compliant with both fftn and rfftn
    if center is None:
        ps = np.abs(np.fft.fftshift((np.fft.fftn(vol))))
        z, y, x = np.indices(ps.shape)
        center = tuple((a - 1) / 2.0 for a in ps.shape[::-1])
        radii = np.sqrt((x - center[0])**2 + (y - center[1])**2 + (z - center[2])**2)
        radii = radii.astype(np.int)
    else:
        ps = np.abs( np.fft.rfftn(vol) )
        if not return_indices:
            x, y, z = np.indices(ps.shape)
            radii = np.sqrt(x**2 + y**2 + z**2)
            radii = radii.astype(np.int)
        else:
            [x, y, z] = np.mgrid[-dim[0]//2+m[0]:(dim[0]-1)//2+1, -dim[1]//2+m[1]:(dim[1]-1)//2+1, 0:dim[2]//2+1]
            x = np.fft.ifftshift(x)
            y = np.fft.ifftshift(y)
            radii = np.sqrt(x**2 + y**2 + z**2)
            radii = radii.astype(np.int)
    radial_profile = np.bincount(radii.ravel(), ps.ravel()) / np.bincount(radii.ravel())
    # exclude corner frequencies
    radial_profile = radial_profile[0:(ps.shape[0]/2)]
    if not return_indices:
        return radial_profile
    else:
        return radial_profile, radii
    
class Vector:
    def __init__(self,input_array):
        self.x = input_array[0]
        self.y = input_array[1]
        self.z = input_array[2]
    def get(self):
        return np.array([self.x,self.y,self.z])
    def magnitude(self):
        return math.sqrt(self.x**2+self.y**2+self.z**2)
    def cap_magnitude(self,cap):
        mag = self.magnitude()
        if mag > cap:
            factor= cap/mag
            return self.scale(factor)
        else:
            return self
    
    def scale(self,scale):
        return Vector(scale*self.get())

def add_Vector(vector_a,vector_b):
        return Vector(vector_a.get() + vector_b.get())

d_type = [('pos',tuple),('vel',tuple),('acc',tuple)]
class PointClass:
    def __init__(self,init_pos):
        self.id = 0
        self.position = Vector(init_pos)
        self.velocity = Vector(np.array([0,0,0]))    
        self.acceleration = Vector(np.array([0,0,0]))
        self.mass = point_mass # Mass factor - not in real units! 
        
        self.lj_acceleration = Vector(np.array([0,0,0]))
        self.map_acceleration = Vector(np.array([0,0,0]))
        self.position_history = [self.position.get()]
        self.velocity_history = [self.velocity.get()]
        self.acceleration_history = [self.acceleration.get()]
        self.lj_acceleration_history = [self.lj_acceleration.magnitude()]
        self.map_acceleration_history = [self.map_acceleration.magnitude()]
    def get_distance_vector(self,target):
        distance_vector = Vector(np.array(self.position.get() - target.position.get()))
        return distance_vector
    
    def angle_wrt_horizontal(self,target):
        return math.atan2(target.position.y - self.position.y, target.position.x - self.position.x)
    
    def velocity_from_acceleration(self,dt):
        vx = self.velocity.x + self.acceleration.x*dt
        vy = self.velocity.y + self.acceleration.y*dt
        vz = self.velocity.z + self.acceleration.z*dt
       # print('velocity: '+str(tuple([vx,vy])))
        self.velocity = Vector(np.array([vx,vy,vz]))
        
    def position_from_velocity(self,dt):
        x = self.position.x + self.velocity.x*dt
        y = self.position.y + self.velocity.y*dt
        z = self.position.z + self.velocity.z*dt
      #  print('position: '+str(tuple([x,y])))
        self.position = Vector(np.array([x,y,z]))
    
    def update_history(self):
        self.position_history.append(self.position.get())
        self.velocity_history.append(self.velocity.get())
        self.acceleration_history.append(self.acceleration.get())
        self.lj_acceleration_history.append(self.lj_acceleration.magnitude())
        self.map_acceleration_history.append(self.map_acceleration.magnitude())
        
def get_acceleration_from_gradient(gx,gy,gz,emmap,g,point,voxelsize):
    tic = time()
    [x,y,z] = [int(point.position.x),int(point.position.y),int(point.position.z)]
    theta_x = gx[z,y,x] / voxelsize
    theta_y = gy[z,y,x] / voxelsize
    theta_z = gz[z,y,x] / voxelsize
    
    acceleration_x = g * theta_x
    acceleration_y = g * theta_y
    acceleration_z = g * theta_z
    #print(point.position.get())
    #print((acceleration_x,acceleration_y))
    
    acceleration = Vector(np.array([acceleration_x,acceleration_y,acceleration_z]))
    #return acceleration,map_slice[y,x]
    time_map.append(time()-tic)
    return acceleration.cap_magnitude(capmagnitude_map),emmap[z,y,x]


def get_acceleration_from_lj_potential(targetpoint,lj_neighbors):
    tic = time()
    #print(len(lj_neighbors))
    '''
    k = 0
    #print(len(lj_neighbors))
    points = [x for x in lj_neighbors if (targetpoint.get_distance_vector(x)).magnitude() != 0]
    #print(len(points))
    r = np.zeros(len(points))
    angles = np.zeros(len(points))
    unit_diff_vector = []
    for point in points:
        diff = targetpoint.get_distance_vector(point)
        #print(str(point.position.get())+str(targetpoint.position.get())+str(diff.get()))
        unit_diff = diff.scale(1/diff.magnitude())
       # print(type(diff.get()))
        unit_diff_vector.append(unit_diff.get())
        r[k] = diff.magnitude()
        #angles[k] = targetpoint.angle_wrt_horizontal(point) #angles in radian
        k += 1
    '''
    lj_neighbors_points = [x.position.get() for x in lj_neighbors]
    distance_vector = targetpoint.position.get() - lj_neighbors_points
    r = np.sqrt(np.einsum('ij->i',distance_vector**2))
    unit_diff_vector = (distance_vector.transpose() / r).transpose()

    
    eps = epsilon
    rm = r_mean*lj_factor
    #print(r[0])
  #  unit_diff_vector = np.array(unit_diff_vector)
    v_lj = eps * ((rm/r)**12 - 2*(rm/r)**6)
   # print(unit_diff_vector.shape)
    #force from v_lj, f=dv/dr
    
    f_r = np.array((12 * eps * rm**6 * (r**6 - rm**6))/r**13)
    
    #print(type(f_r))
    #print(type(unit_diff_vector))
    
    f_r_vector = np.array([np.array(f_r[k])*np.array(unit_diff_vector[k]) for k in range(len(lj_neighbors))])
    #print(f_r_vector)
    fx = -f_r_vector[:,0]
    fy = -f_r_vector[:,1]
    fz = -f_r_vector[:,2]
    '''
    #fx_sum = sum_array_isfinite(fx)
    #fy_sum = sum_array_isfinite(fy)
    #fz_sum = sum_array_isfinite(fz)
    '''
    
    ax = fx.sum() / targetpoint.mass
    ay = fy.sum() / targetpoint.mass
    az = fz.sum() / targetpoint.mass
    acc = Vector(np.array([ax,ay,az]))
    #return acc,v_lj.sum()
    
    time_lj.append(time()-tic)
    return acc.cap_magnitude(capmagnitude_lj),v_lj.sum()

def plot_map_and_points(map_slice,points):
    colors = ['b','g','r','c','m','y','k','w']
    k = 0
    plt.imshow(map_slice)
    nearest_neighbors = get_nearest_neighbor_distance(points)
    for point in points:
        if nearest_neighbors[k] > r_mean:
            plt.plot(point.position.x,point.position.y,'w'+'.')
        else:
            plt.plot(point.position.x,point.position.y,'r'+'.')
        k += 1

def plot_3d_points(points):
    points_position = points_position = [x.position.get() for x in points]
    ax.scatter3D(*zip(*points_position),color='b')
    
def quiver_plot(map_slice,points):
    plt.imshow(map_slice)
    plt.title('Black is acceleration due to LJ, Red is acceleration due to gradient')
    for point in points:
        (X,Y) = point.position.get()
        (U_lj,V_lj) = point.lj_acceleration.get()
        (U_map,V_map) = point.map_acceleration.get()
        
        plt.quiver(X,Y,U_lj,V_lj,color='r',scale=1,scale_units='x')
        plt.quiver(X,Y,U_map,V_map,color='k',scale=1,scale_units='x')


    
def get_neighborhood(points):
    tic = time()
    np_points = np.array([list(x.position.get()) for x in points])
    neighborhood = {}
    
    tree = KDTree(np_points)
    for i in range(len(points)):
        ind = tree.query_radius(np_points[i:i+1],r=r_mean*3)[0]
        d,ix = tree.query(np_points[i:i+1],k=2)
        #print(d[0][1])
        #print(ind[0])
        ind = np.delete(ind,np.where(ind==i))
        neighborhood[i]=[d[0][1],ind]
    #number_of_contacts = tree.two_point_correlation(np_points,r_mean)
    time_neighbor.append(time()-tic)
    return neighborhood
    
toc = time()
mrc = mrcfile.open('emd5778_unfiltered.mrc')
emmap = mrc.data
emmap = emmap.clip(min=0)
#emmap = normalize(emmap,0,100)
voxelsize = 1
real_vsize = mrc.voxel_size.x

#map_slice = normalize(emmap[:,:,slice_index],0,100)
#map_slice = get_simple_landscape(fx,fy,N=50)
#map_slice = normalize(map_slice,0,100)

mrc = mrcfile.open('emd5778_unfiltered_confidenceMap.mrc')
mask = mrc.data
#mask_slice = mask[:,:,slice_index]

(width,height,depth) = emmap.shape
all_inside_mask = np.asarray(np.where(mask==1)).T.tolist()

gz,gy,gx = np.gradient(emmap)

num_points = calculate_number_of_atoms(mask,protein_density)
print(num_points)
points = [PointClass(np.array([x[2]*real_vsize,x[1]*real_vsize,x[0]*real_vsize])) for x in random.sample(all_inside_mask,num_points)]
for k in range(len(points)):
    points[k].point_id = k
    
#points = [PointClass(tuple([x[1],x[0]])) for x in np.load('random_selection_3.npy')]
#points = [PointClass((10,20)),PointClass((12,26)),PointClass((39,29)),PointClass((35,31)),PointClass((14,21)),PointClass((15,6)),PointClass((16,13)),PointClass((11,31))]
#points = [PointClass(point1),PointClass(point2)]
colors = ['b','g','r','c','m','y','k','w']
k = 0

map_potential_list = []
lj_potential_list = []
tot_potential_list = []

map_acceleration_list=  []
lj_acceleration_list = []

dt = delta_time

#fig = plt.figure()
#ax = fig.add_subplot(111, projection='3d')
#camera = Camera(fig)
point_analysis = {}
num_kicklist = []
print("Total mask volume (in pix^3) "+str(mask.sum()))
print("Total atoms to place "+str(num_points))

for iter in range(iterrange):
    #plot_3d_points(points)
    #camera.snap()
    sum_of_map_potential = 0
    neighborhood = get_neighborhood(points)
    small_distances = [d[0] for d in neighborhood.values() if d[0] <= r_mean]
    num_kicklist.append(len(small_distances))

    point_id = 0
    for point in points:
        lj_neighbors = [points[k] for k in neighborhood[point_id][1]]
        
        gradient_acceleration,map_potential = get_acceleration_from_gradient(gx,gy,gz,emmap, g, point=point, voxelsize=1.2156)
        if len(lj_neighbors)==0:
            lj_potential_acceleration,lj_potential = Vector(np.array([0,0,0])),0
        else:
            lj_potential_acceleration,lj_potential = get_acceleration_from_lj_potential(point, lj_neighbors)
        
        gradient_acceleration,lj_potential_acceleration = gradient_acceleration.scale(scale_map),lj_potential_acceleration.scale(scale_lj)
        acceleration = add_Vector(gradient_acceleration,lj_potential_acceleration)
        # add friction 
        point.acceleration = add_Vector(acceleration, point.velocity.scale(-friction))
        
        # Next few lines are for analysis
     #   point.lj_acceleration = lj_potential_acceleration
     #   point.map_acceleration = gradient_acceleration
        
        sum_of_map_potential += map_potential 
        #sum_of_lj_potential += lj_potential
        point_id += 1
      
    
         
    #tot_potential_list.append(sum_of_lj_potential+sum_of_map_potential)

    #plt.imshow(map_slice)
    #plot_point_series_and_map(map_slice,points)
    
    
    map_potential_list.append(sum_of_map_potential/num_points)
    #lj_potential_list.append(sum_of_lj_potential)


    
    
    for point in points:
        point.velocity_from_acceleration(dt)        
        #print(str(iter)+", "+str(k)+", "+str(point.lj_acceleration.magnitude())+", "+str(point.map_acceleration.magnitude())+", "+str(point.acceleration.magnitude()))
        point.position_from_velocity(dt)
     #   point.update_history()
     
     
     
    print(str(iter)+": Num kicked = "+str(len(small_distances))+": Avg map potential= "+str(sum_of_map_potential/num_points))    

time_lj = np.array(time_lj)
time_map = np.array(time_map)
time_neighbor = np.array(time_neighbor) 
total_time = time()-toc

#outfilename = 'gradient_and_lj_potential_3d.gif'
#animation = camera.animate()
#panimation.save(outfilename)

gemmi_model = convert_to_gemmi_model(points,1);
write_pdb(gemmi_model,'3j5p_pseudomodel_pdb.pdb')
path_to_refmap_script = '/home/alok/dev/locscale/source/prepare_locscale_input.py'
emmap_path = 'emd5778_unfiltered.mrc'
model_path = '3j5p_pseudomodel_pdb.pdb'
mask_path = 'emd5778_unfiltered_confidenceMap.mrc'
# convert_to_map 
command_line = "phenix.python "+path_to_refmap_script+" -mc "+model_path+" -em "+emmap_path+" -ma "+mask_path
run(command_line)
modelmap_path = '3j5p_pseudomodel_pdb_4locscale.mrc'
model_map = mrcfile.open(model_path).data()
radial_profile = compute_radial_profile(model_map)

plt.plot(radial_profile)
    