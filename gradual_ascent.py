# -*- coding: utf-8 -*-
"""
Created on Thu Oct  1 19:03:16 2020

@author: abharadwaj1
"""
from headers import *

point_mass = 1
epsilon = 1
min_dist = 1.5
scale_lj = 1
scale_map = 1
delta_time = 0.05
total_iterations = 100
slice_index = 120
num_points_fraction = 0.1
capmagnitude_map = 100
capmagnitude_lj = 400
protein_density = 1.35
g = 3
lj_factor = 1.5
friction = 15
samples = 7
fx = 4
fy = 4


#point1 = tuple(np.random.randint(5,45,(1,2))[0])
#point2 = tuple(np.random.randint(5,45,(1,2))[0])
#point1 = (25,34)
#point2 = (25,32)

mrc = mrcfile.open('emd5778_unfiltered.mrc')
emmap = mrc.data
voxelsize = 1

map_2d = normalize(emmap[:,:,slice_index],0,100)
#map_2d = get_simple_landscape(fx,fy,N=50)
#map_2d = normalize(map_2d,0,100)

mrc = mrcfile.open('emd5778_unfiltered_confidenceMap.mrc')
mask = mrc.data
mask_slice = mask[:,:,slice_index]

(width,height) = map_2d.shape
all_inside_mask = np.asarray(np.where(mask_slice==1)).T.tolist()

num_points = calculate_number_of_atoms(mask_slice,protein_density)

dt = delta_time
arranged_points = []
contacts = [] 
min_distance = []
similarity = []
rmsd = []
map_values = []
cross_correlations = []
# Generate reference points from an initial random sample



for sample in range(samples):
    points = [PointClass(tuple([x[1]*voxelsize,x[0]*voxelsize])) for x in random.sample(all_inside_mask,num_points)]
    new_points,num_contacts_s,map_value_iterations = main_solver(map_2d,points,g=g,friction=friction,total_iterations=total_iterations,capmagnitude_lj=capmagnitude_lj,capmagnitude_map=capmagnitude_map,lj_factor=lj_factor,min_dist=min_dist,return_contacts=True,return_map_value=True, return_type='pointclass')
    #new_points = main_solver_analysis(map_2d,points,reference_points,lj_factor=lj_factor,friction=friction,total_iterations=200,return_type='array')
    arranged_points.append(new_points)
    contacts.append(num_contacts_s)
    map_values.append(map_value_iterations)

    



#final_converged.append(converged)
#final_converged_image.append(get_image_of_points(ensemble_array))

#plt.imshow(add_all_converged_samples),plt.scatter(*zip(*final_converged),color='r')


    '''
plt.figure()
for i in range(samples):
    plt.scatter(cross_correlations[i][-1],map_values[i][-1],color='k')
    plt.xlabel('Converged Cross correlations')
    plt.ylabel(' Converged Map Value')
    
plt.figure()
for i in range(samples):
    plt.plot(cross_correlations[i],'k')
    plt.xlabel('Iterations')
    plt.ylabel('Cross correlations')
    '''
plt.figure()
for i in range(samples):
    plt.plot(map_values[i],'k')
    plt.xlabel('Iterations')
    plt.ylabel('Map value')

plt.figure() 
for i in range(samples):
    plt.plot(contacts[i],'k')
    plt.xlabel('Iterations')
    plt.ylabel('Number of contacts')

plt.figure()    
plot_position_history_and_map(map_2d,arranged_points[0])

